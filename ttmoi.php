function isAuthenticated() {

    if (isset($_SESSION["auth"]) && $_SESSION["auth"] == 1) {
        return true;
    }

    if (isset($_COOKIE["auth_user_id"])) {
        $id = $_COOKIE["auth_user_id"];

        //
        // t�i t?o th�ng tin cho session

        $sql = "select * from users where f_ID = $id";
        $rs = load($sql);
        if ($rs->num_rows == 0) {
            return false;
        }

        $row = $rs->fetch_assoc();

        $u = array();
        $u["f_Username"] = $row["f_Username"];
        $u["f_ID"] = $row["f_ID"];
        $u["f_Name"] = $row["f_Name"];
        $u["f_Email"] = $row["f_Email"];
        $u["f_DOB"] = $row["f_DOB"];
        $u["f_Permission"] = $row["f_Permission"];

        $_SESSION["auth"] = 1;
        $_SESSION["auth_user"] = $u;

        return true;
    }

    return false;
}