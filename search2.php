﻿else {
                    ?>
                    <div class="row">
                        <form id="f" action="" method="post">
                            <input type="hidden" id="txtProId" name="txtProId" />
                        </form>
                        <?php
                        while ($row = $rs->fetch_assoc()) {
                            ?>
                            <div class="col-sm-6">
                                <div class="thumbnail">
                                    <a href="imgs/sp/<?php echo $row["ProID"]; ?>/main.jpg" data-lightbox="<?php echo $row["ProID"]; ?>" data-title="<?php echo $row["ProName"]; ?>">
                                        <img src="imgs/sp/<?php echo $row["ProID"]; ?>/main_thumbs.jpg" alt="...">
                                    </a>                                    
                                    <div class="caption">
                                        <h4><?php echo $row["ProName"]; ?></h4>
                                        <h4><?php echo number_format($row["Price"]); ?></h4>
                                        <p>
                                            <?php echo $row["TinyDes"]; ?>
                                        </p>
                                        <p>
                                            <a href="index.php?act=details&id=<?php echo $row["ProID"]; ?>" class="btn btn-primary" role="button">
                                                Chi tiết
                                            </a>
                                            <?php if (isAuthenticated()) { ?>
                                                <a href="#" class="btn btn-success" role="button" onclick="setProId(<?php echo $row["ProID"]; ?>);">
                                                    <i class="fa fa-cart-plus"></i>
                                                    Đặt hàng
                                                </a>
                                                <?php
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }