﻿
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 title">
                        Thông tin đăng nhập
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5 col-md-offset-1">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="txtUID" name="txtUID" placeholder="Tên đăng nhập">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="password" class="form-control" id="txtPWD" name="txtPWD" placeholder="Mật khẩu">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-7">
                                <label style="font-weight: normal">
                                    <input type="checkbox" name="chkRememberMe" /> Ghi nhớ
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <button type="submit" class="btn btn-primary pull-right" name="btnLogin" id="btnLogin">
                                    <i class="fa fa-check"></i> Đăng nhập
                                </button>
                            </div>
                        </div>
                    </div>

    </div>