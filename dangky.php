<div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span>Error</span>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 title">
                        Th�ng tin dang nh?p
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="form-group">
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="txtUID" placeholder="T�n dang nh?p">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <input type="password" class="form-control" name="txtPWD" placeholder="M?t kh?u">
                            </div>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" name="txtConfirmPWD" placeholder="Nh?p l?i m?t kh?u">
                            </div>
                        </div>
                    </div>
                </div>