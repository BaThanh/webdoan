<div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Danh mục</h3>
        </div>

        <div class="list-group">
            <?php
            
            require_once './dbHelper.php';
            
            $sql = "select * from categories";
            $rs = load($sql);
				while ($row = $rs->fetch_assoc()) {
					$id = $row["CatID"];
					$name = $row["CatName"];
					?>
					<a class="list-group-item" href="index.php?act=products&id=<?php echo $id; ?>"><?php echo $name; ?></a>
					<?php
				}	
            ?>
        </div>
    </div>
</div>